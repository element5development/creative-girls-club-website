<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<header class="primary-nav-container">

		<?php 
			$phone_link = get_field('primary_phone','option');
			$phone_link = preg_replace('/[^0-9]/', '', $phone_link);
		?>

		<a id="phone-toggle" href="tel:+1<?php echo $phone_link; ?>">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" aria-labelledby="title" aria-describedby="desc">
				<path data-name="layer1" d="M48.5 40.2a4.8 4.8 0 0 0-6.5 1.3c-2.4 2.9-5.3 7.7-16.2-3.2S19.6 24.4 22.5 22a4.8 4.8 0 0 0 1.3-6.5L17 5.1c-.9-1.3-2.1-3.4-4.9-3S2 6.6 2 15.6s7.1 20 16.8 29.7S39.5 62 48.4 62s13.2-8 13.5-10-1.7-4-3-4.9z" fill="#FF00A6"/>
			</svg>
		</a>

    <a class="brand" href="<?= esc_url(home_url('/')); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/CGC-logo.png" />
		</a>

		<button id="search-toggle-2">
			<svg width="31" height="32" viewBox="0 0 31 32">
				<defs>
					<path id="307fa" d="M1323.92 56.85a10.92 10.92 0 1 1 0-21.85 10.92 10.92 0 0 1 0 21.85z"/>
					<path id="307fb" d="M1331.23 53.9l8.08 8.08"/>
				</defs>
				<g>
					<g transform="translate(-1311 -33)">
						<g>
							<use fill="#e9ff00" fill-opacity="0" stroke="#e9ff00" stroke-miterlimit="50" stroke-width="4" xlink:href="#307fa"/>
						</g>
						<g>
							<use fill="#e9ff00" fill-opacity="0" stroke="#e9ff00" stroke-miterlimit="50" stroke-width="6" xlink:href="#307fb"/>
						</g>
					</g>
				</g>
			</svg>
		</button>
    
		<?php get_template_part('template-parts/navigation/social'); ?>
</header>