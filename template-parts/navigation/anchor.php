<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>


<nav class="nav-anchor">
	<a class="brand" href="<?= esc_url(home_url('/')); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/CGC-logo.png" />
	</a>

	<?php if (has_nav_menu('primary_nav')) :
		wp_nav_menu(['theme_location' => 'primary_nav']);
	endif; ?>

	<a href="tel:+1<?php the_field('primary_phone','option'); ?>" class="button"><?php the_field('primary_phone','option'); ?></a>

	<button id="search-toggle">
		<svg width="31" height="32" viewBox="0 0 31 32">
			<defs>
				<path id="307fa" d="M1323.92 56.85a10.92 10.92 0 1 1 0-21.85 10.92 10.92 0 0 1 0 21.85z"/>
				<path id="307fb" d="M1331.23 53.9l8.08 8.08"/>
			</defs>
			<g>
				<g transform="translate(-1311 -33)">
					<g>
						<use fill="#e9ff00" fill-opacity="0" stroke="#e9ff00" stroke-miterlimit="50" stroke-width="4" xlink:href="#307fa"/>
					</g>
					<g>
						<use fill="#e9ff00" fill-opacity="0" stroke="#e9ff00" stroke-miterlimit="50" stroke-width="6" xlink:href="#307fb"/>
					</g>
				</g>
			</g>
		</svg>
		<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
			<defs>
				<style>
					.cls-1{fill:none;stroke:#e9ff00;stroke-miterlimit:10;stroke-width:5px}
				</style>
			</defs>
			<path class="cls-1" d="M2 2l20 20M22 2L2 22"/>
		</svg>
	</button>

</nav>
<section id="search">
	<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		<input type="search" class="search-field"
				placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
				value="<?php echo get_search_query() ?>" name="s"
				title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
		<button type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>">
			<svg width="31" height="32" viewBox="0 0 31 32">
				<defs>
					<path id="307fa" d="M1323.92 56.85a10.92 10.92 0 1 1 0-21.85 10.92 10.92 0 0 1 0 21.85z"/>
					<path id="307fb" d="M1331.23 53.9l8.08 8.08"/>
				</defs>
				<g>
					<g transform="translate(-1311 -33)">
						<g>
							<use fill="#000" fill-opacity="0" stroke="#000" stroke-miterlimit="50" stroke-width="4" xlink:href="#307fa"/>
						</g>
						<g>
							<use fill="#000" fill-opacity="0" stroke="#000" stroke-miterlimit="50" stroke-width="6" xlink:href="#307fb"/>
						</g>
					</g>
				</g>
			</svg>
		</button>
	</form>
	<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
		<defs>
			<style>
				.cls-3{fill:none;stroke:#000;stroke-miterlimit:10;stroke-width:5px}
			</style>
		</defs>
		<path class="cls-3" d="M2 2l20 20M22 2L2 22"/>
	</svg>
</section>
<button id="menu-toggle">
	<svg width="30" height="25" viewBox="0 0 30 25">
		<path d="M0 10h30v5H0v-5zM0 0h30v5H0V0zm0 20h30v5H0v-5z" fill="#e9ff00" fill-rule="evenodd"/>
	</svg>
	<svg width="28" height="27" viewBox="0 0 28 27">
		<path d="M17.61 13.433l9.681 9.681-3.818 3.819-9.682-9.682-9.5 9.5-3.818-3.818 9.5-9.5-9.41-9.41L4.38.204l9.41 9.41L23.383.023 27.2 3.84l-9.592 9.592z" fill="#e9ff00" fill-rule="evenodd"/>
	</svg>
</button>