<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>
<article class="post-preview">

	<?php
	if( have_rows('instruction_documentation') ):
		while ( have_rows('instruction_documentation') ) : the_row();
			$title = get_sub_field('title');
			$number = get_sub_field('revision_number');
			$download = get_sub_field('download');
			?>
			<a href="<?php echo $download; ?>">
				<div class="pdf-card">
					<svg width="148" height="149" xmlns="http://www.w3.org/2000/svg">
						<g transform="translate(2 3)" fill="none" fill-rule="evenodd">
							<circle stroke="#FF00A6" stroke-width="5" cx="72" cy="71.5" r="71.5"/>
							<g stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">
								<path d="M56.258 49.998h-6.326v52.054H89.33v-5.914"/>
								<path d="M95.658 44.083h-39.4v5.915H89.33v46.14h6.327z"/>
								<path d="M89.331 96.138v-46.14H56.26"/>
								<path d="M56.259 70.11h26.17V56.612H56.26zM56.258 96.138h10.57V76.2h-10.57zM71.861 96.138H82.43V76.2H71.86z"/>
							</g>
						</g>
					</svg>
					<h5><?php echo $title; ?></h5>
					<p>version <?php echo $number; ?></p>
					<a href="<?php echo $download; ?>" class="button">Download PDF</a>
				</div>
			</a>
			<?php
		endwhile;
	else :
		// no rows found
	endif;
	?>

</article>
<hr>