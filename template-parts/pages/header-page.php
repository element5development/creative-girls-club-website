<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php 
	if ( get_field('background_image') ) {
		$background = get_field('background_image');
		$backgroundURL = $background['url'];
	} else {
		$backgroundURL = get_stylesheet_directory_uri() . '/dist/images/default-header.jpg';
	}
	$button_one = get_field('button_one');
	$button_two = get_field('button_two');
?>

<?php 
	if ( get_field('background_image') ) {
?>
<section class="page-title title-section" style="background-image: url(<?php echo $backgroundURL; ?>);">
<?php 
	} else {
?>
<section class="page-title title-section" style="background-image: url(<?php echo $backgroundURL; ?>);">
<?php 
	}
?>
	<div class="block">
		<?php if ( get_field('title') ) { ?>

			<?php if( get_field('title_color') == 'white' ): ?>
				<h1 style="color: #ffffff;"><?php the_field('title'); ?></h1>
			<?php else: ?>
				<h1><?php the_field('title'); ?></h1>
			<?php endif; ?>

			<?php if( get_field('subheader_color') == 'black' ): ?>
				<h2 style="color: #000000;"><?php the_field('subheader'); ?></h2>
			<?php else: ?>
				<h2><?php the_field('subheader'); ?></h2>
			<?php endif; ?>

		<?php } else { ?>

			<?php if( get_field('title_color') == 'white' ): ?>
				<h1 style="color: #ffffff;"><?php the_title(); ?></h1>
			<?php else: ?>
				<h1><?php the_title(); ?></h1>
			<?php endif; ?>

		<?php } ?>
		<?php if ( $button_one ) { ?>
			<a target="<?php echo $button_one['target']; ?>" href="<?php echo $button_one['url']; ?>" class="button is-third"><?php echo $button_one['title']; ?></a>
		<?php } ?>
		<?php if ( $button_two  ) { ?>
			<a target="<?php echo $button_two['target']; ?>" href="<?php echo $button_two['url']; ?>" class="button"><?php echo $button_two['title']; ?></a>
		<?php } ?>
	</div>
	<?php if ( is_front_page() ) { ?>
		<div class="circle-gradient"></div>
	<?php } ?>
</section>